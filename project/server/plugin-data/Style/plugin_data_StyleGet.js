
'use strict';

const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StyleGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    StackComponentsFactory.getStacks((stacks) => {
      this.analyze(stacks);
    });
  }
  
  analyze(stacks) {
    let indexStack = 0;
    const protocols = [
      ['unknown', {
        index: indexStack++,
        textColor: 'black',
        textProtocolColor: 'black',
        protocolColor: '#C0C0C0',
        protocolBackgroundColor: 'rgba(102, 102, 102, 0.3)'
      }]
    ];
    stacks.forEach((path, stack) => {
      const currentStackApi = require(`${stack}-style`);
      const currentStyle = new currentStackApi(indexStack++);
      protocols.push([stack, currentStyle]);
    });
    return this.expectAsynchResponseSuccess(protocols);
  }
}


module.exports = StyleGet;
